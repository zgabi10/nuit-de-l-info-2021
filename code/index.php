<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">

  <!--  Parti styles --->
  <link rel="stylesheet" href="../style/DarkMode.css">
  <link rel="stylesheet" href="../style/menu_deroulant.css">
  <link rel="stylesheet" href="../style/Positionnement.css">
  <link rel="stylesheet" href="../style/styles.css">


  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="icon" type="../image/png" href="../img/logo.png" />



  <title>Sauveteurs bénévoles</title>

<script>
	function DarkMode() {
		
		var element = document.body;
		element.classList.toggle("dark-mode");

		document.getElementById("BoutonSoleil").src="../img/lune.png";
	
	}


	function CaseCocher(){
		if (document.getElementById("bateaux").checked == true &&  document.getElementById("Personne").checked == true){ 
				// permet de faire des evenement 
				alert("les deux ") ; 
				
			}

			if (document.getElementById("bateaux").checked == true)
			{
				alert('Case Bateau Cocher ');
				 

			}
			if (document.getElementById("Personne").checked == true)
			{
				alert('Case Cocher Personne ');
				

			}

		}
	
</script>
</head>

	<?php 
		echo "<input type=HIDDEN value='valeurs '>" ; 
	?>
	<body>

		<header>		
			
			<h1>Accueil</h1>
			<section>
				<a href="login.html">Login <img src="../img/login.png" alt="Une image de connexion"></a>
			</section>
			<button onclick = DarkMode()> 
				<img id="BoutonSoleil" src = "../img/soleil.png" >
			</button>
		<aside>
        <!-- Menu dépliant -->
			<nav>
				<label for="menu-cb" class="menu-label">
					<svg viewBox="0 0 32 32" fill="#666">
					<rect x="0" y="4" rx="3" ry="3" width="32" height="3" />
					<rect x="0" y="14" rx="3" ry="3" width="32" height="3" />
					<rect x="0" y="24" rx="3" ry="3" width="32" height="3" />
					</svg>
				</label>
				<input id="menu-cb" type="checkbox" class="menu-cb">
				<nav class="menu-nav">
					<ul>
					<li class="menu-item"><a href="Accueil.html">Accueil</a></li>
					<li class="menu-item"><a href="">Personnages</a></li>
					<li class="menu-item"><a href="#page3">Bateau</a></li>
					<li class="menu-item"><a href="#page3">Communauté</a></li>
					</ul>
				</nav>
    	</aside>


		</header>
		<!-- -->
		<main> 
			<section>
				<p>
				Bienvenue sur le site des sauveteurs du dunkerquois.
				Ce site rend hommage aux femmes, hommes et enfants qui ont réalisé des actes 
				de sauvetages en milieu aquatique.
				Ces sauveteurs, habitants du dunkerquois (de Bray-Dunes à Grand-Fort-Philippe), ont 
				participé à plus de 900 sauvetages en mer et plus de 1100 sauvetages individuels.
				Œuvrant avec courage, abnégation et souvent au mépris du risque ils méritent 
				amplement que leurs actes soient pérennisés.
				</p>
			</section>
			<?php 
			include ('Connexion.php');
			include ('functionRecherche.php') ; 
			Connexionlogin();


			


				affichageRecherche($bateau); 	
			
			?> 
			<div>
				<input type="checkbox" id="bateaux" name="bateaux" onclick="CaseCocher()" checked>
				<label for="bateaux">Bateaux</label>
				<input type="checkbox" id="Personne" name="Personne" onclick="CaseCocher()" checked>
				<label for="Personne">Personne</label>
			</div>
			<input type= "Submit" value="Rechercher"> 



				


		</main>



		<footer>


		</footer>
	</body>

</html>






